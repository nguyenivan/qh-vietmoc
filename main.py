import wsgiref.handlers
import urllib

from google.appengine.ext import webapp
from google.appengine.api import urlfetch
from google.appengine.api import mail


class MainHandler(webapp.RequestHandler):
    def get(self):
        self.redirect('/index.html')


class ContactHandler(webapp.RequestHandler):
	def post(self):
		user_address = self.request.get("mail")
		if mail.is_email_valid(user_address):
			receiver_address = "admin@vietmoc.com"
			sender_address = "noreply@qh-vietmoc.appspotmail.com"
			name = urllib.unquote(self.request.get('name'))
			subject = urllib.unquote(self.request.get('subject'))
			message = urllib.unquote(self.request.get('message'))
			body = "Sent by " + user_address
			body = body + """
---------
"""
			body = body + message + """

---------
Sent via www.vietmoc.com
"""
			mail.send_mail("VietMoc Website <"+sender_address+">", receiver_address, subject, body)
			if self.request.get('copy') == '1':
				body = """
Thank for your message via www.vietmoc.com. That is message you sent:
---------

""" + message
				mail.send_mail("VietMoc Website <"+sender_address+">", name+" <"+user_address+">", subject, body)
			self.response.out.write("Sent successfully")
		else :
			self.response.out.write("Email invalid")


def main():
    application = webapp.WSGIApplication([
						('/', MainHandler),
						('/contact', ContactHandler)
					], debug=True)
    wsgiref.handlers.CGIHandler().run(application)


if __name__ == '__main__':
    main()
